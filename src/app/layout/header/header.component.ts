import { Component, OnInit } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  faBars = faBars;
  status: boolean;

  constructor() {}

  ngOnInit() {}

  toggleMenu(status) {
    if (status) {
      document.getElementById("menu").style.opacity = "1";
      document.getElementById("menu").style.position = "static";
    } else {
      document.getElementById("menu").style.opacity = "0";
      document.getElementById("menu").style.position = "absolute";
    }
  }
}
