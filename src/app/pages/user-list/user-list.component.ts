import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { User } from '@model/user';
import { UserService } from '@service/user.service';
import { MessageService } from 'primeng/api';

import { UserDataDialogComponent } from '../user-data-dialog/user-data-dialog.component';

// import { userDataDialog } from '../user-data-dialog'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers: [MessageService]
})

// @Directive({ selector: "app-user-data-dialog" })
// export class ChildDirective {}
export class UserListComponent implements OnInit {
  @ViewChild(UserDataDialogComponent, { static: false })
  private UserDataDialogComponent: UserDataDialogComponent;
  public users: User[];
  display = false;

  id: number;
  constructor(
    private userService: UserService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.reload();
    this.route.params.forEach((params: Params) => {
      const id = +params['id'];
      if (id) {
        if (this.userService.getUserById(id)) {
          this.showDialog(id);
        }
      }
    });
  }

  reload() {
    this.userService.getUsers().subscribe(res => (this.users = res));
  }

  showDialog(id: number) {
    this.id = id;
    if (this.id) {
      this.router.navigate(['/users', this.id]);
    }
    this.display = true;
  }

  addSingleNotif(id: number) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Értesítés',
      detail: id + '. számú felhasználó törölve'
    });
  }

  deleteUser(id: number) {
    this.userService.deleteUser(id);
    this.addSingleNotif(id);
  }

  cancelData() {
    this.display = false;
    this.router.navigate(['/users']);
  }

  saveData(id) {
    if (id) {
      this.userService.modifyUser(
        id,
        this.UserDataDialogComponent.localName,
        this.UserDataDialogComponent.localEmail,
        this.UserDataDialogComponent.localAddress,
        this.UserDataDialogComponent.localBirthday
      );
    } else {
      this.userService.createUser(
        this.UserDataDialogComponent.localName,
        this.UserDataDialogComponent.localEmail,
        this.UserDataDialogComponent.localAddress,
        this.UserDataDialogComponent.localBirthday
      );
    }
    this.router.navigate(['/users']);
    this.display = false;
  }
}
