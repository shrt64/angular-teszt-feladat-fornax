import { Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { UserService } from '@service/user.service';

@Component({
  selector: 'app-user-data-dialog',
  templateUrl: './user-data-dialog.component.html',
  styleUrls: ['./user-data-dialog.component.scss']
})
export class UserDataDialogComponent implements OnInit, OnChanges {
  localName: string;
  localEmail: string;
  localAddress: string;
  localBirthday: Date;

  constructor(private userService: UserService) {}

  @Input() id: number;
  @Input() display: boolean;

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    const currentDisplay: SimpleChange = changes.display;
    if (this.id) {
      const obj = this.userService.getUserById(this.id);
      if (this.display === true) {
        this.localName = obj.name;
        this.localEmail = obj.email;
        this.localAddress = obj.address;
        if (obj.birthdate) {
          this.localBirthday = new Date(obj.birthdate);
        }
      }
    } else {
      this.localName = '';
      this.localEmail = '';
      this.localAddress = '';
      this.localBirthday = null;
    }
  }

  public showlocalName() {
    return this.localName;
  }
}
