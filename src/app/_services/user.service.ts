import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@model/user';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: "root"
})
export class UserService {
    private userList: User[] = JSON.parse(localStorage.getItem("userList"));

    constructor(private http: HttpClient) { }

    getUsers(): Observable<User[]> {
        return of(this.userList);
    }

    getUserById(id: number): any {
        return this.userList[this.userList.findIndex(el => el.id === id)];
    }

    modifyUser(user: User): any {
        const objectIndex = this.userList.find(el => el.id === user.id);
        this.userList[objectIndex].name = name;
        this.userList[objectIndex].email = email;
        this.userList[objectIndex].address = address;
        this.userList[objectIndex].birthdate = birthdate;
        localStorage.setItem("userList", JSON.stringify(this.userList));
    }

    deleteUser(id: number): any {
        this.userList.splice(
            this.userList.findIndex(el => el.id === id),
            1
        );
        console.log(this.userList);

        localStorage.setItem("userList", JSON.stringify(this.userList));
    }

    createUser(
        name: string,
        email: string,
        address: string,
        birthdate: Date
    ): any {
        let max = 0;
        this.userList.forEach(character => {
            if (character.id > max) {
                max = character.id;
            }
        });
        let newItem: User[] = [
            {
                id: max + 1,
                name: name,
                email: email,
                address: address,
                birthdate: birthdate
            }
        ];
        this.userList.push(newItem[0]);
        localStorage.setItem("userList", JSON.stringify(this.userList));
    }
}
